/*
modetolist

A simple mode number to list item converter

Takes a list of mode numbers, then figures out which items in a list the modes
refer to.

Example: if you have three options in the mode file, hello, goodbye, and greet,
they would be specified in a file separated by new lines. The program would take
that file's path as the first argument. This is our control file. It then would
take numbers from STDIN. These numbers should reflect the UNIX mode style. For
each option, add 2^n where n is that option number. So in this case, hello would
be 1, goodbye would be 2, and greet would be 4. So 6 would cause the program to
print greet and goodbye. 7 would print greet goodbye and hello. 1 would print
hello, and so on.

This is written with the intent of sending out a page with options available to
be selected. The selected options would then be summed in the way specified
above, and then each response is piled in a text file, \n separated. This file
then gets piped into this program, turning up the results in a list.

From there, the user may sort and/or count the results, or whatever they very
well like, since the output is sent to stdout.

Written October 15 2017 by Jim Read (gitlab.com/CannonContraption)
*/

#include<iostream>
#include<fstream>
#include<vector>
using namespace std;
int pow(
    int to_raise,
    int power)
{
    int final_number = 1;
    for(
        int iterations = 0;
        iterations < power;
        iterations ++)
    {
        final_number *= to_raise;
    }
    return final_number;
}

int main(
    int argc,
    char* argv[])
{
    if(argc<1)
        return 1;
    vector<string> options;
    fstream specfile;
    string currentspec;
    specfile.open(
        argv[1],
        fstream::in);
    while(!specfile.eof())
    {
        getline(specfile,currentspec);
        options.emplace_back(currentspec);
    }
    int num_specs = options.size();
    int current_num;
    int current_power;
    while(
        !cin.fail() &&
        !cin.eof())
    {
        cin>>current_num;
        for(
            int option_number = num_specs;
            option_number >= 0;
            option_number --)
        {
            current_power = pow(2, option_number);
            if(
                current_num -
                    current_power
                >= 0)
            {
                current_num -= current_power;
                cout<<options[option_number]<<endl;
            }
            if(current_num == 0)
                break;
        }
    }
    return 0;
}
